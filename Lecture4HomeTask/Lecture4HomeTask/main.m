//
//  main.m
//  Lecture4HomeTask
//
//  Created by DB on 31.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import "Animal.h"
#import "Dog.h"
#import "Cat.h"
#import "Cow.h"
#import "MonoSoundSystem.h"
#import "SoundSystem2in1.h"
#import "SoundSystem5in1.h"

#import "NSMutableDictionary+SafeSetValueForKey.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        //Animal chorus lol
        //=============================================
        MonoSoundSystem *monoSoundSystem = [MonoSoundSystem new];
        SoundSystem2in1 *soundSystem2in1 = [SoundSystem2in1 new];
        SoundSystem5in1 *soundSystem5in1 = [SoundSystem5in1 new];
        Dog *dog = [Dog new];
        Cat *cat = [Cat new];
        Cow *cow = [Cow new];
        NSArray *arrayOfAnimals = @[ dog,cat,cow ];
        NSArray *arrayOfSoundSystems = @[monoSoundSystem,soundSystem2in1,soundSystem5in1];
        
        for (int i = 0; i < 20; i++)
        {
            Animal *animal = arrayOfAnimals[rand()%3];
            [animal setSoundSystem:arrayOfSoundSystems[rand()%3]];
            [animal voice];
        }
        //===============================================
        
        //MyCategory
        //===============================================
        NSMutableDictionary *dictionary = [NSMutableDictionary new];
        [dictionary setObject:dog forKey:@"dog"];
        [dictionary setObject:cat forKey:@"cat"];
        [dictionary setObject:cow forKey:@"cow"];
        [dictionary setSafeObject:nil forKey:@"animal"];
        [dictionary setSafeObject:monoSoundSystem forKey:nil];
        
        NSLog(@"dictionary: %@",dictionary);
        
        //===============================================
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
