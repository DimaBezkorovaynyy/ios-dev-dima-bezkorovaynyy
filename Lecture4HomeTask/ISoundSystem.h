//
//  ISoundSystem.h
//  Lecture4HomeTask
//
//  Created by DB on 31.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ISoundSystem <NSObject>

@required
-(void) makeSound:(NSString *)sound;

@end
