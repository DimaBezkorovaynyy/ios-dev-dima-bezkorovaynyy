//
//  Animal.h
//  Lecture4HomeTask
//
//  Created by DB on 31.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IAnimal.h"
#import "ISoundSystem.h"


@interface Animal : NSObject <IAnimal>

@property (weak) id <ISoundSystem> soundSystem;

-(void)voice;


@end
