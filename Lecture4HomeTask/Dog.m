//
//  Dog.m
//  Lecture4HomeTask
//
//  Created by DB on 31.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import "Dog.h"

@implementation Dog

-(void) voice
{
    [self.soundSystem makeSound:@"Woof"];
}

@end
