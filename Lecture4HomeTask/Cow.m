//
//  Cow.m
//  Lecture4HomeTask
//
//  Created by DB on 31.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import "Cow.h"

@implementation Cow

-(void)voice
{
    [self.soundSystem makeSound:@"Moo"];
}

@end
