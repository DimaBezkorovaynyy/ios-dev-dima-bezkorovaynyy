//
//  AppDelegate.h
//  testProject
//
//  Created by DB on 17.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

