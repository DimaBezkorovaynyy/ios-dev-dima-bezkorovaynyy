//
//  NSMutableDictionary+SafeSetValueForKey.h
//  Lecture4HomeTask
//
//  Created by DB on 31.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (SafeSetValueForKey)

-(void)setSafeObject:(id)anObject forKey:(id<NSCopying>)aKey;

@end
