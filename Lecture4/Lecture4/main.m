//
//  main.m
//  Lecture4
//
//  Created by DB on 28.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import  "Animals.h"
#import "SoundSystem.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        Animals *animals = [Animals new];
        Dog *dog = [Dog new];
        Cat *cat = [Cat new];
        
        SoundSystem *soundSystem = [SoundSystem new];
        
        animals.soundSystem = soundSystem;
        dog.soundSystem = soundSystem;
        cat.soundSystem = soundSystem;
        
        NSArray *array = @[animals,dog,cat];
        
        for (Animals *animal in array)
        {
            [animal voice];
        }
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
