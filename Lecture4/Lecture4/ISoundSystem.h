//
//  ISoundSystem.h
//  Lecture4
//
//  Created by DB on 28.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ISoundSystem <NSObject>

@required
-(void)makeSound:(NSString *)sound;

@end
