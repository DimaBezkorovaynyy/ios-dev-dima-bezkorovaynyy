//
//  NSMutableDictionary+SafeSetValueForKey.m
//  Lecture4HomeTask
//
//  Created by DB on 31.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import "NSMutableDictionary+SafeSetValueForKey.h"

@implementation NSMutableDictionary (SafeSetValueForKey)

-(void)setSafeObject:(id)anObject forKey:(id<NSCopying>)aKey
{
    if (anObject && aKey)
    {
        [self setObject:anObject forKey:aKey];
    }
}

@end
