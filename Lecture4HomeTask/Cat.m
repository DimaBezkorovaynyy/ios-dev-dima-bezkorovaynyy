//
//  Cat.m
//  Lecture4HomeTask
//
//  Created by DB on 31.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import "Cat.h"

@implementation Cat

-(void)voice
{
    [self.soundSystem makeSound:@"Meow"];
}

@end
