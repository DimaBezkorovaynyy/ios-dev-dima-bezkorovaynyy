//
//  Animals.m
//  Lecture4
//
//  Created by DB on 28.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import "Animals.h"

@implementation Animals

-(void)voice
{
    [self.soundSystem makeSound:@"Animals voice"];//NSLog(@"Animals voice");
}

@end

@implementation Dog

-(void)voice
{
    [self.soundSystem makeSound:@"Dog voice"];//NSLog(@"Dog voice");
}

@end

@implementation Cat

-(void)voice
{
    [self.soundSystem makeSound:@"Cat voice"];//NSLog(@"Cat voice");
}
@end
