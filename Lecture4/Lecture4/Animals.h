//
//  Animals.h
//  Lecture4
//
//  Created by DB on 28.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnimalProtocol.h"
#import "SoundSystem.h"
#import "ISoundSystem.h"

@interface Animals : NSObject<AnimalProtocol>

@property (weak) id<ISoundSystem> soundSystem;

@end


@interface Dog : Animals//NSObject<AnimalProtocol>

@end

@interface Cat : Animals//NSObject<AnimalProtocol>



@end
