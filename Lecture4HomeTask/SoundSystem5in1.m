//
//  SoundSystem5in1.m
//  Lecture4HomeTask
//
//  Created by DB on 31.03.16.
//  Copyright © 2016 DB. All rights reserved.
//

#import "SoundSystem5in1.h"

@implementation SoundSystem5in1

-(void)makeSound:(NSString *)sound
{
    NSLog(@"Loud sound: %@", sound);
}

@end
